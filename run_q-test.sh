#!/bin/bash

do_q220(){
    
    #asetup AtlasProduction,19.1.3.7
    project=q220
    echo "Submitting " $project
    if [ -d $project ]; then rm -rf $project; fi
    mkdir -p $project
    cd $project
    echo $HOSTNAME > host
    nohup Reco_tf.py --conditionsTag all:CONDBR2-BLKPA-2015-02 --beamType 'cosmics' --ignoreErrors 'False' --autoConfiguration='everything' --maxEvents '30' --AMITag 'q220' --preExec  'all:from CaloRec.CaloCellFlags import jobproperties;jobproperties.CaloCellFlags.doLArHVCorr=False;jobproperties.CaloCellFlags.doPileupOffsetBCIDCorr.set_Value_and_Lock(False);from InDetRecExample.InDetJobProperties import InDetFlags;InDetFlags.doInnerDetectorCommissioning.set_Value_and_Lock(True);InDetFlags.useBroadClusterErrors.set_Value_and_Lock(False);DQMonFlags.doStreamAwareMon=False;DQMonFlags.enableLumiAccess=False;from JetRec.JetRecFlags import jetFlags;jetFlags.useTracks=False;DQMonFlags.doCTPMon=False;' --geometryVersion all:ATLAS-R2-2015-02-00-00  --inputBSFile='/afs/cern.ch/work/c/cohm/public/PROC/TestFiles/data15_cos.00251363.physics_IDCosmic.merge.RAW._lb0057._SFO-ALL._0001.1' --outputAODFile 'myAOD.pool.root' &> log &
    cd ../
}


do_q221(){
    echo "Rel_5 Hack!!!"
    #asetup AtlasProduction,20.1.4.11
    project=q221
    echo "Submitting " $project
    if [ -d $project ]; then rm -rf $project; fi
    mkdir -p $project
    cd $project
    echo $HOSTNAME > host
    nohup Reco_tf.py --AMITag 'q221' --ignoreErrors 'False' --autoConfiguration='everything' --maxEvents '10' --conditionsTag all:OFLCOND-RUN12-SDR-25 --digiSeedOffset2 '1' --DataRunNumber '222525' --preExec  'RAWtoESD:from InDetRecExample.InDetJobProperties import InDetFlags;InDetFlags.doLowBetaFinder=False' 'ESDtoAOD:TriggerFlags.AODEDMSet="AODFULL"'  'all:rec.Commissioning.set_Value_and_Lock(True);from AthenaCommon.BeamFlags import jobproperties;jobproperties.Beam.numberOfCollisions.set_Value_and_Lock(0.);from LArROD.LArRODFlags import larRODFlags;larRODFlags.nSamples.set_Value_and_Lock(4)'  'HITtoRDO:from Digitization.DigitizationFlags import digitizationFlags;digitizationFlags.overrideMetadata+=["SimLayout","PhysicsList"];'  'RAWtoESD:from TriggerJobOpts.TriggerFlags import TriggerFlags;TriggerFlags.triggerMenuSetup="MC_pp_v5";from CaloRec.CaloCellFlags import jobproperties;jobproperties.CaloCellFlags.doLArCellEmMisCalib=False;' --digiSeedOffset1 '1' --geometryVersion all:ATLAS-R2-2015-02-01-00 --inputHITSFile='/afs/cern.ch/atlas/project/rig/valid1.110401.PowhegPythia_P2012_ttbar_nonallhad.simul.HITS.e3099_s2082_tid01614220_00/HITS.01614220._000553.pool.root.1' --outputAODFile 'myAOD.pool.root' &> log &
    cd ../
}


do_q222(){
    echo "Rel_5 Hack!!!" #'RAWtoESD:from InDetRecExample.InDetJobProperties import InDetFlags;InDetFlags.doLowBetaFinder=False'
    #asetup AtlasProduction,19.1.3.7
    project=q222
    echo "Submitting " $project
    if [ -d $project ]; then rm -rf $project; fi
    mkdir -p $project
    cd $project
    nohup Reco_tf.py --maxEvents '30' --AMITag 'q222' --autoConfiguration='everything' --conditionsTag all:COMCOND-BLKPA-RUN1-07 --preExec 'RAWtoESD:from InDetRecExample.InDetJobProperties import InDetFlags;InDetFlags.doLowBetaFinder=False' 'all:rec.doTrigger=False' --inputBSFile='/afs/cern.ch/atlas/project/rig/referencefiles/dataStreams_high_mu/data12_8TeV/data12_8TeV.00209109.physics_JetTauEtmiss.merge.RAW._lb0186._SFO-1._0001.1' --outputAODFile 'myAOD.pool.root' &> log &
   cd ../
}


do_rtt(){
    project=rtt
    echo "Submitting " $project
    if [ -d $project ]; then rm -rf $project; fi
    mkdir -p $project
    cd $project
    echo $HOSTNAME > host
    nohup athena.py -c 'RunningRTT=TRUE;menu="MC_pp_v5";sliceName="tau";jp.AthenaCommonFlags.EvtMax.set_Value_and_Lock(100);jp.Rec.OutputLevel=WARNING;LVL1OutputLevel=WARNING;HLTOutputLevel=WARNING; dsName="/eos/atlas/atlascerngroupdisk/trig-daq/validation/test_data/mc12_8TeV.147818.Pythia8_AU2CTEQ6L1_Ztautau.digit.RDO.e1176_s1479_s1470_d822_tid01379810_00";fileRange=[30,33]' -b TrigAnalysisTest/testAthenaTrigRDOtoAOD.py &> log &
    cd ../
}

do_q223(){
    echo "Rel_5 Hack!!!" #'RAWtoESD:from InDetRecExample.InDetJobProperties import InDetFlags;InDetFlags.doLowBetaFinder=False'
    project=q223
    echo "Submitting " $project
    if [ -d $project ]; then rm -rf $project; fi
    mkdir -p $project
    cd $project
    echo $HOSTNAME > host
    nohup Reco_tf.py --conditionsTag all:CONDBR2-ES1PA-2015-04 --beamType 'collisions' --ignoreErrors 'False' --autoConfiguration='everything' --maxEvents '30' --AMITag 'q223' --postExec  'all:from AthenaCommon.AppMgr import ServiceMgr;import MuonRPC_Cabling.MuonRPC_CablingConfig;ServiceMgr.MuonRPC_CablingSvc.RPCTriggerRoadsfromCool=False' --preExec  'all:larCondFlags.OFCShapeFolder.set_Value_and_Lock("4samples3bins17phases");from InDetRecExample.InDetJobProperties import InDetFlags;InDetFlags.useBeamConstraint.set_Value_and_Lock(False);InDetFlags.doMinBias=True;InDetFlags.useDCS.set_Value_and_Lock(False);DQMonFlags.doStreamAwareMon=False;DQMonFlags.enableLumiAccess=False;DQMonFlags.doCTPMon=False;' 'RAWtoESD:from InDetRecExample.InDetJobProperties import InDetFlags;InDetFlags.doLowBetaFinder=False' --geometryVersion all:ATLAS-R2-2015-03-01-00 --inputBSFile='/afs/cern.ch/atlas/project/rig/referencefiles/QTests-Run2/data15_comm.00264034.physics_MinBias.daq.RAW._lb0644._SFO-6._0001.data' --outputAODFile 'myAOD.pool.root' &> log &
    cd ../
}

do_q431(){
    echo "Rel_5 Hack!!!" #'RAWtoESD:from InDetRecExample.InDetJobProperties import InDetFlags;InDetFlags.doLowBetaFinder=False'
    project=q431
    echo "Submitting " $project
    if [ -d $project ]; then rm -rf $project; fi
    mkdir -p $project
    cd $project
    echo $HOSTNAME > host
    nohup Reco_tf.py --conditionsTag all:CONDBR2-BLKPA-2015-05 --beamType 'collisions' --ignoreErrors 'False' --autoConfiguration='everything' --maxEvents '30' --AMITag 'q431' --postExec  'r2e:topSequence.LArNoisyROAlg.Tool.BadChanPerFEB=30' --preExec  'all:DQMonFlags.enableLumiAccess=False;DQMonFlags.doCTPMon=False;from MuonRecExample.MuonRecFlags import muonRecFlags;muonRecFlags.useLooseErrorTuning.set_Value_and_Lock(True);' 'RAWtoESD:from InDetRecExample.InDetJobProperties import InDetFlags;InDetFlags.doLowBetaFinder=False' --geometryVersion all:ATLAS-R2-2015-03-01-00 --ignorePatterns='ToolSvc.InDetSCTRodDecoder.+ERROR.+Unknown.+offlineId.+for.+OnlineId' --inputBSFile='/afs/cern.ch/user/m/magdac/public/TagTests/data15_13TeV.00266919.physics_Main.merge.RAW._lb0250._SFO-1._0001.1' --outputAODFile 'myAOD.pool.root' &> log &
    cd ../
}

do_q220;
do_q221;
do_q222;
# # do_rtt;
do_q223;
do_q431;
