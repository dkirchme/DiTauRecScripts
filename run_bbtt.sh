#!/bin/sh

INPUT_DIR=$1
INPUT_FILE=$2
OUTPUT_DIR=$3
OUTPUT_FILE=$(echo $INPUT_FILE | sed s/ESD/AOD/)


project='run_'$INPUT_FILE
echo "Submitting " $project
if [ -d $project ]; then rm -rf $project; fi
mkdir -p $project
cd $project
    

Reco_tf.py --steering 'RAWtoESD:in-RDO,in+RDO_TRIG,in-BS' --conditionsTag 'default:OFLCOND-RUN12-SDR-31' --pileupFinalBunch '6' --numberOfHighPtMinBias '0.12268057' --autoConfiguration 'everything' --numberOfLowPtMinBias '39.8773194' --preInclude 'HITtoRDO:Digitization/ForceUseOfPileUpTools.py,SimulationJobOptions/preInclude.PileUpBunchTrainsMC15_2015_25ns_Config1.py,RunDependentSimData/configLumi_run222525_v1.py' --postExec 'all:CfgMgr.MessageSvc().setError+=["HepMcParticleLink"]' 'HITtoRDO:job.StandardPileUpToolsAlg.PileUpTools["MergeMcEventCollTool"].OnlySaveSignalTruth=True;job.StandardPileUpToolsAlg.PileUpTools["MdtDigitizationTool"].LastXing=150' 'RDOtoRDOTrigger:from AthenaCommon.AlgSequence import AlgSequence;AlgSequence().LVL1TGCTrigger.TILEMU=True;from AthenaCommon.AppMgr import ServiceMgr;import MuonRPC_Cabling.MuonRPC_CablingConfig;ServiceMgr.MuonRPC_CablingSvc.RPCTriggerRoadsfromCool=False' --postInclude 'default:RecJobTransforms/UseFrontier.py' --preExec 'all:rec.Commissioning.set_Value_and_Lock(True);from AthenaCommon.BeamFlags import jobproperties;jobproperties.Beam.numberOfCollisions.set_Value_and_Lock(20.0);from LArROD.LArRODFlags import larRODFlags;larRODFlags.NumberOfCollisions.set_Value_and_Lock(20);larRODFlags.nSamples.set_Value_and_Lock(4);larRODFlags.doOFCPileupOptimization.set_Value_and_Lock(True);larRODFlags.firstSample.set_Value_and_Lock(0);larRODFlags.useHighestGainAutoCorr.set_Value_and_Lock(True)' 'RAWtoESD:from CaloRec.CaloCellFlags import jobproperties;jobproperties.CaloCellFlags.doLArCellEmMisCalib=False' 'ESDtoAOD:from DiTauRec.DiTauRecFlags import diTauFlags; diTauFlags.diTauRecJetSeedPt.set_Value_and_Lock(100000)' 'ESDtoAOD:from DiTauRec.DiTauRecFlags import diTauFlags; diTauFlags.doVtxFinding.set_Value_and_Lock(True)' 'ESDtoAOD:TriggerFlags.AODEDMSet="AODSLIM"' --triggerConfig 'MCRECO:DBF:TRIGGERDBMC:2013,7,11' --geometryVersion 'default:ATLAS-R2-2015-03-01-00' --numberOfCavernBkg '0' --inputESDFile=$INPUT_DIR$INPUT_FILE --outputAODFile $OUTPUT_FILE &> log # && cp $OUTPUT_FILE $OUTPUT_DIR
