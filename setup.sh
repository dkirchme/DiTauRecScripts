#!/bin/bash

export release=20.1.5.10
if [ ! -z ${1+x} ]; then export release=$1 ; else echo "using default release" ;fi

shopt -s expand_aliases
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/
alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'
alias localSetupDQ2Client='source ${ATLAS_LOCAL_ROOT_BASE}/packageSetups/atlasLocalDQ2ClientSetup.sh --dq2ClientVersion ${dq2ClientVersionVal}'
alias localSetupGcc='source ${ATLAS_LOCAL_ROOT_BASE}/packageSetups/atlasLocalGccSetup.sh --gccVersion ${gccVersionVal}'

setupATLAS --quiet
# localSetupDQ2Client --quiet --skipConfirm
voms-proxy-init -voms atlas 
echo "setting up release: "$release

# if [[ "$release" =~ ".X" ]] ; then 
#     asetup $release,latest_copied_release,here
# else
#     asetup $release,here
# fi

asetup $release,here


localSetupGcc gcc481_x86_64_slc6

getKerberos

# module load parallel

export PATH=$PATH:/raid1/users/kirchmeier/scripts/tauRecToolsTest/