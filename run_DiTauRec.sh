#!/bin/sh

INPUT_SAMPLE='mc15_13TeV.303367.MadGraphPythia8EvtGen_A14NNPDF23LO_RS_G_hh_bbtt_hh_c10_M2000.recon.ESD.e4438_s2608_r6869_tid06575941_00/'
INPUT_DIR='/ZIH.fast/users/kirchmeier/ESD_G_hh_bbttt/'$INPUT_SAMPLE

# INPUT_FILES='ESD.06575941._000037.pool.root.1
# 			 ESD.06575941._000030.pool.root.1
# 			 ESD.06575941._000011.pool.root.1
# 			 ESD.06575941._000003.pool.root.1
# 			 ESD.06575941._000035.pool.root.1'

INPUT_FILES='ESD.06575941._000001.pool.root.1'
# INPUT_FILES='ESD.06575941._000001.pool.root.1
# 			 ESD.06575941._000002.pool.root.1
# 			 ESD.06575941._000003.pool.root.1
# 			 ESD.06575941._000004.pool.root.1
# 			 ESD.06575941._000005.pool.root.1
# 			 ESD.06575941._000006.pool.root.1
# 			 ESD.06575941._000007.pool.root.1
# 			 ESD.06575941._000008.pool.root.1
# 			 ESD.06575941._000009.pool.root.1
# 			 ESD.06575941._000010.pool.root.1
# 			 ESD.06575941._000011.pool.root.1
# 			 ESD.06575941._000012.pool.root.1
# 			 ESD.06575941._000013.pool.root.1
# 			 ESD.06575941._000014.pool.root.1
# 			 ESD.06575941._000015.pool.root.1
# 			 ESD.06575941._000016.pool.root.1
# 			 ESD.06575941._000017.pool.root.1
# 			 ESD.06575941._000018.pool.root.1
# 			 ESD.06575941._000019.pool.root.1
# 			 ESD.06575941._000020.pool.root.1
# 			 ESD.06575941._000021.pool.root.1
# 			 ESD.06575941._000022.pool.root.1
# 			 ESD.06575941._000023.pool.root.1
# 			 ESD.06575941._000024.pool.root.1
# 			 ESD.06575941._000025.pool.root.1'
# INPUT_FILES='ESD.06575941._000026.pool.root.1
# 			 ESD.06575941._000027.pool.root.1
# 			 ESD.06575941._000028.pool.root.1
# 			 ESD.06575941._000029.pool.root.1
# 			 ESD.06575941._000030.pool.root.1
# 			 ESD.06575941._000031.pool.root.1
# 			 ESD.06575941._000032.pool.root.1
# 			 ESD.06575941._000033.pool.root.1
# 			 ESD.06575941._000034.pool.root.1
# 			 ESD.06575941._000035.pool.root.1
# 			 ESD.06575941._000036.pool.root.1
# 			 ESD.06575941._000037.pool.root.1
# 			 ESD.06575941._000038.pool.root.1
# 			 ESD.06575941._000039.pool.root.1
# 			 ESD.06575941._000040.pool.root.1
# 			 ESD.06575941._000041.pool.root.1
# 			 ESD.06575941._000042.pool.root.1
# 			 ESD.06575941._000043.pool.root.1
# 			 ESD.06575941._000044.pool.root.1
# 			 ESD.06575941._000045.pool.root.1
# 			 ESD.06575941._000046.pool.root.1
# 			 ESD.06575941._000047.pool.root.1
# 			 ESD.06575941._000048.pool.root.1
# 			 ESD.06575941._000049.pool.root.1'

OUTPUT_DIR='/raid1/users/kirchmeier/samples/'$INPUT_SAMPLE

parallel -j 50% --eta --load 50% --nice 19 --joblog $OUTPUT_DIR'parallel.log' "run_bbtt.sh $INPUT_DIR {1} $OUTPUT_DIR" ::: $INPUT_FILES

echo $INPUT_FILES